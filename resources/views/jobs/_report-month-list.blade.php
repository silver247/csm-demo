@extends('layouts.app')

@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Jobs</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">#รายการรายงานประจำเดือน</span>
        </div>
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            รายการรายงานประจำเดือน
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="/jobs/{{$job->id}}/monthly-report/create" role="tab">
                                    สร้างรายงานประจำเดือน
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>เดือน</th>
                                <th>ปี</th>
                                <th>จำนวนลูกจ้าง</th>
                                <th>จำนวนชั่วโมงการทำงาน</th>
                                <th>จำนวนอุบัติการณ์</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($report_list->count() <= 0)
                            <tr>
                                <td colspan="5" class="text-center">No Data</td>
                            </tr>
                            @else
                            @foreach($report_list as $report)
                            <tr>
                                <td>{{$report->report_month}}</td>
                                <td>{{$report->report_year}}</td>
                                <td>{{$report->employees}}</td>
                                <td>{{$report->working_hours}}</td>
                                <td>{{$report->incident_count}}</td>
                                <td><a href="/jobs/{{$job->id}}/monthly-report/detail/{{$report->id}}" class="btn btn-info">View</a></td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    {{ $report_list->links() }}
                </div>
            </div>
        </div>
    </div>

</div>

<!-- end:: Content -->
@endsection
