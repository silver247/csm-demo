@extends('layouts.app')

@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Jobs</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">#รายละเอียดรายงานประจำเดือน</span>
        </div>
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            รายละเอียดรายงานประจำเดือน
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                    <div class="kt-portlet__body">
                        <div class="form-group form-group-last">
                            <div class="alert alert-secondary" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                <div class="alert-text">
                                    กรุณากรอกข้อมูลในช่องที่มี <span class="text-danger">*</span> ให้ครบทุกช่อง
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        <div class="form-group row">
                            
                            <div class="col-6">
                                <label for="description" >เดือน<span class="text-danger">*</span></label>
                                <input class="form-control" value="{{$report->report_month}}" readonly>
                            </div>
                            <div class="col-6">
                                <label for="description" >ปี<span class="text-danger">*</span></label>
                                <input class="form-control" value="{{$report->report_year + 543}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">จำนวนลูกจ้าง <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="{{$report->employees}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">จำนวนชั่วโมงการทำงาน <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="{{$report->working_hours}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">จำนวนการเกิดอุบัติการณ์ <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="{{$report->incident_count}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-12">
                                    <a href="/jobs/{{$id}}/monthly-report" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

</div>

<!-- end:: Content -->
@endsection