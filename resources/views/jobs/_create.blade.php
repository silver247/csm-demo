@extends('layouts.app')

@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Jobs</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">#Created</span>
        </div>
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            สร้างงานใหม่
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="POST" action="/jobs/create">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group form-group-last">
                            <div class="alert alert-secondary" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                <div class="alert-text">
                                    กรุณากรอกข้อมูลในช่องที่มี <span class="text-danger">*</span> ให้ครบทุกช่อง
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="form-group row">
                            <label for="dept_owner">หน่วยงานผู้ว่าจ้าง (ฝ่ายหรือสูงกว่า) <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <select class="form-control" name="owner_csct" id="owner_csct">
                                    @foreach($department_list as $dept)
                                    <option value="{{$dept->cost_center}}">{{$dept->description}} ({{$dept->short_description}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">งานที่ว่าจ้าง <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="text" value="" placeholder="ระบุชื่องานที่ว่าจ้าง" name="job_name" id="job_name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" >ระยะเวลาที่ว่าจ้าง<span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="kt_daterangepicker_1" readonly placeholder="Select time" />
                                <input type="hidden" id="job_start" name="job_start">
                                <input type="hidden" id="job_end" name="job_end">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vendor_type_id">ประเภทกิจการ <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <select class="form-control" name="vendor_type_id" id="vendor_type_id">
                                    @foreach($vendor_list as $vendor)
                                    <option value="{{$vendor->id}}">{{$vendor->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vendor_employee">จำนวนลูกจ้าง(คน) <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="" placeholder="จำนวนลูกจ้าง(คน)" name="vendor_employee" id="vendor_employee">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="developer" >ส่งมอบพื้นที่ <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="incl_location" value="1"> ใช่
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="incl_location" value="0"> ไม่
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="dept_owner">สถานที่ทำงาน<span class="text-danger">*</span></label>
                                <div class="kt-radio-list">
                                    @foreach($job_location_list as $location)
                                    <label class="kt-radio">
                                        <input type="radio" name="job_location_master_id" value="{{$location->id}}"> {{$location->description}}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                                <input class="form-control" type="text" value="" placeholder="ระบุเมื่อตัวเลือกอื่นๆ" name="job_location_other" id="job_location_other">
                            </div>
                            <div class="col-6">
                                <label for="dept_owner">ลักษณะงาน<span class="text-danger">*</span></label>
                                <div class="kt-radio-list">
                                    @foreach($job_type_list as $type)
                                    <label class="kt-radio">
                                        <input type="radio" name="job_type_master_id" value="{{$type->id}}"> {{$type->description}}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                                <input class="form-control" type="text" value="" placeholder="ระบุเมื่อตัวเลือกอื่นๆ" name="job_type_other" id="job_type_other">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="dept_owner">ลักษณะกิจกรรมที่ปฏิบัติ<span class="text-danger">*</span></label>
                                <div class="kt-checkbox-list">
                                    @foreach($job_activity_list as $activity)
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="job_activities[]" value="{{$activity->id}}"> {{$activity->description}}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="dept_owner">เครื่องมือ เครื่องจักร อุปกรณ์<span class="text-danger">*</span></label>
                                <div class="kt-checkbox-list">
                                    @foreach($job_equipment_list as $equipment)
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="job_equipments[]" value="{{$equipment->id}}"> {{$equipment->description}}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="dept_owner">สภาพแวดล้อมในการทำงาน<span class="text-danger">*</span></label>
                            </div>
                            @foreach($environment_list as $column)
                            <div class="col-lg-3 col-md-6">
                                <label>{{$column->description}}</label>

                                @foreach ($column->job_environments()->get() as $row)
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="job_environments[]" value="{{$row->id}}"> {{$row->description}}
                                        <span></span>
                                    </label>
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="dept_owner">การประเมินความเสี่ยง<span class="text-danger">*</span></label>
                            </div>
                            <table class="table table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th colspan="4">เกณฑ์ประเมิน</th>
                                        <th rowspan="2">คะแนน</th>
                                    </tr>
                                    <tr>
                                        <th>หัวข้อประเมิน</th>
                                        <th>#</th>
                                        <th>ระดับ</th>
                                        <th width='70%'>ความหมาย</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($risk_list as $row)
                                    <tr>
                                        @if($loop->index % $row->count_topic($row->topic->id) === 0)
                                        <td rowspan="3"  class="text-center"> {{$row->topic->description}}</td>
                                        @endif
                                        
                                        <td>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio">
                                                    <input type="radio" name="risks[{{$row->topic->id}}]" value="{{$row->id}}" required>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">{{$row->level->description}}</td>
                                        <td>{{$row->description}}</td>
                                        <td class="text-center">{{$row->score}}</td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <a href="/" class="btn btn-secondary">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- end:: Content -->
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $('#owner_csct').select2();
        // minimum setup
        $('#kt_daterangepicker_1, #kt_daterangepicker_1_modal').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function (start, end, label) {
            $('#job_start').val(start.format('YYYY-MM-DD'));
            $('#job_end').val(end.format('YYYY-MM-DD'));
        });
    });
</script>
@endpush
