@extends('layouts.app')

@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Jobs</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">#Detail</span>
        </div>
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            รายละเอียดงาน
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="form-group form-group-last">
                        <div class="alert alert-secondary" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                            <div class="alert-text">
                                {{$job->risk_group()->name}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dept_owner">หน่วยงานผู้ว่าจ้าง (ฝ่ายหรือสูงกว่า) <span class="text-danger">*</span></label>
                        <div class="col-12">
                            <input class="form-control" type="text" value="{{$job->department->description}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="job_name">งานที่ว่าจ้าง <span class="text-danger">*</span></label>
                        <div class="col-12">
                            <input class="form-control" type="text" value="{{$job->job_name}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" >ระยะเวลาที่ว่าจ้าง<span class="text-danger">*</span></label>
                        <div class="col-12">
                            <input class="form-control" type="text" value="{{$job->job_start}} - {{$job->job_end}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vendor_type_id">ประเภทกิจการ <span class="text-danger">*</span></label>
                        <div class="col-12">
                            <input class="form-control" type="text" value="{{$job->vendor->description}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vendor_type_id">ชื่อบริษัท</label>
                        <div class="col-12">
                            <input class="form-control" type="text" value="{{$job->vendor_name}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vendor_type_id">เลขที่สัญญา </label>
                        <div class="col-12">
                            <input class="form-control" type="text" value="{{$job->vendor_contract_id}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vendor_employee">จำนวนลูกจ้าง(คน) <span class="text-danger">*</span></label>
                        <div class="col-12">
                            <input class="form-control" type="number" value="{{$job->vendor_employee}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="developer" >ส่งมอบพื้นที่ <span class="text-danger">*</span></label>
                        <div class="col-12">
                            <div class="kt-radio-inline">
                                <label class="kt-radio">
                                    <input type="radio" name="incl_location" value="1" @if($job->incl_location) checked @endif> ใช่
                                    <span></span>
                                </label>
                                <label class="kt-radio">
                                    <input type="radio" name="incl_location" value="0" @if(!$job->incl_location) checked @endif> ไม่
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6">
                            <label for="dept_owner">สถานที่ทำงาน<span class="text-danger">*</span></label>
                            <div class="kt-radio-list">
                                @foreach($job_location_list as $location)
                                <label class="kt-radio">
                                    <input type="radio" name="job_location_master_id" disabled value="{{$location->id}}" @if($job->job_location_master_id == $location->id) checked @endif> {{$location->description}}
                                    <span></span>
                                </label>
                                @endforeach
                            </div>
                            <input class="form-control" type="text" value="{{$job->job_location_other}}" >
                        </div>
                        <div class="col-6">
                            <label for="dept_owner">ลักษณะงาน<span class="text-danger">*</span></label>
                            <div class="kt-radio-list">
                                @foreach($job_type_list as $type)
                                <label class="kt-radio">
                                    <input type="radio" name="job_type_master_id" disabled value="{{$type->id}}" @if($job->job_type_id == $type->id) checked @endif> {{$type->description}}
                                    <span></span>
                                </label>
                                @endforeach
                            </div>
                            <input class="form-control" type="text" value="{{$job->job_type_other}}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6">
                            <label for="dept_owner">ลักษณะกิจกรรมที่ปฏิบัติ<span class="text-danger">*</span></label>
                            <div class="kt-checkbox-list">
                                @foreach($job_activity_list as $activity)
                                <label class="kt-checkbox">
                                    <input type="checkbox" name="job_activities[]" disabled value="{{$activity->id}}" 
                                           @if($job->activities()->where('job_activity_masters_id', $activity->id)->exists()) checked @endif> {{$activity->description}}
                                    <span></span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="dept_owner">เครื่องมือ เครื่องจักร อุปกรณ์<span class="text-danger">*</span></label>
                            <div class="kt-checkbox-list">
                                @foreach($job_equipment_list as $equipment)
                                <label class="kt-checkbox">
                                    <input type="checkbox" name="job_equipments[]" disabled value="{{$equipment->id}}"
                                           @if($job->equipments()->where('job_equipment_id', $equipment->id)->exists()) checked @endif> {{$equipment->description}}
                                    <span></span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="dept_owner">สภาพแวดล้อมในการทำงาน<span class="text-danger">*</span></label>
                        </div>
                        @foreach($environment_list as $column)
                        <div class="col-lg-3 col-md-6">
                            <label>{{$column->description}}</label>

                            @foreach ($column->job_environments()->get() as $row)
                            <div class="kt-checkbox-list">
                                <label class="kt-checkbox">
                                    <input type="checkbox" name="job_environments[]" disabled value="{{$row->id}}"
                                           @if($job->environments()->where('job_environment_id', $row->id)->exists()) checked @endif> {{$row->description}}
                                    <span></span>
                                </label>
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <label for="dept_owner">การประเมินความเสี่ยง<span class="text-danger">*</span></label>
                        </div>
                        <table class="table table-bordered">
                            <thead class="text-center">
                                <tr>
                                    <th colspan="4">เกณฑ์ประเมิน</th>
                                    <th rowspan="2">คะแนน</th>
                                </tr>
                                <tr>
                                    <th>หัวข้อประเมิน</th>
                                    <th>#</th>
                                    <th>ระดับ</th>
                                    <th width='70%'>ความหมาย</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($risk_list as $row)
                                <tr>
                                    @if($loop->index % $row->count_topic($row->topic->id) === 0)
                                    <td rowspan="3"  class="text-center"> {{$row->topic->description}}</td>
                                    @endif

                                    <td>
                                        <div class="kt-radio-list">
                                            <label class="kt-radio kt-radio--success">
                                                <input type="radio" name="risks[{{$row->topic->id}}]" value="{{$row->id}}" disabled 
                                                       @if($job->risks()->where('risk_master_id', $row->id)->exists()) checked @endif>
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">{{$row->level->description}}</td>
                                    <td>{{$row->description}}</td>
                                    <td class="text-center">{{$row->score}}</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                                @if($job->statusNormal())
                                <a href="/jobs/{{$job->id}}/edit" class="btn btn-success">Edit</a>
                                @endif
                                <a href="/jobs/{{$job->id}}/exportTOR" class="btn btn-danger">Export TOR</a>
                                <a href="/" class="btn btn-secondary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- end:: Content -->
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $('#owner_csct').select2();
        // minimum setup
        $('#kt_daterangepicker_1, #kt_daterangepicker_1_modal').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function (start, end, label) {
            $('#job_start').val(start.format('YYYY-MM-DD'));
            $('#job_end').val(end.format('YYYY-MM-DD'));
        });
    });
</script>
@endpush
