@extends('layouts.app')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Jobs List</h3>
        </div>

    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="row">
        @if($job_list->count() <= 0)
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                        <div class="alert-text">
                            There is no job yet.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @else
        @foreach($job_list as $job)

        <div class="col-xl-6">

            <!--begin:: Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin::Widget -->
                    <div class="kt-widget kt-widget--project-1">
                        <div class="kt-widget__head">
                            <div class="kt-widget__label">
                                <div class="kt-widget__media">
                                    <span class="kt-media kt-media--lg kt-media--circle">
                                        <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--rounded kt-badge--bold">{{mb_substr($job->job_name ,0,1)}}</span>
                                        <!--<img src="assets/media/project-logos/1.png" alt="image">-->
                                    </span>
                                </div>
                                <div class="kt-widget__info">
                                    <a href="#" class="kt-widget__title">
                                        {{$job->job_name}}
                                    </a>
                                    <span class="kt-widget__desc">
                                        {{$job->job_description}}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__body">
                            <div class="kt-widget__stats">
                                <div class="kt-widget__item">
                                    <span class="kt-widget__date">
                                        วันที่เริ่ม
                                    </span>
                                    <div class="kt-widget__label">
                                        <span class="btn btn-label-brand btn-sm btn-bold btn-upper">{{$job->job_start}}</span>
                                    </div>
                                </div>
                                <div class="kt-widget__item">
                                    <span class="kt-widget__date">
                                        วันที่สิ้นสุด
                                    </span>
                                    <div class="kt-widget__label">
                                        <span class="btn btn-label-danger btn-sm btn-bold btn-upper">{{$job->job_end}}</span>
                                    </div>
                                </div>
                                <div class="kt-widget__item">
                                    <span class="kt-widget__date">หน่วยงานผู้ว่าจ้าง</span>
                                    <div class="kt-widget__label">
                                    <span class="btn btn-label-info btn-sm btn-bold btn-upper">{{$job->owner_csct}}</span>
                                    </div>
                                </div>
                            </div>
                            <span class="kt-widget__text">
                            </span>
                            <div class="kt-widget__content">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__subtitle">รหัสเอกสาร</span>
                                    <span class="kt-widget__value">{{$job->document_ref}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__footer">
                            <div class="kt-widget__wrapper">
                                <div class="kt-widget__section">
                                    <a href="/jobs/{{$job->id}}" class="btn btn-brand btn-sm btn-upper btn-bold">details</a> &nbsp;
                                    <a href="/jobs/{{$job->id}}/exportTOR" class="btn btn-success btn-sm btn-upper btn-bold">Export TOR</a>
                                </div>
                                <div class="kt-widget__section">
                                    <a href="/jobs/{{$job->id}}/monthly-report" class="btn btn-warning btn-sm btn-upper btn-bold">รายงานประจำเดือน</a> &nbsp;
                                    <a href="/jobs/{{$job->id}}/closejob-survey" class="btn btn-danger btn-sm btn-upper btn-bold">รายงานปิดงาน</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end::Widget -->
                </div>
            </div>

            <!--end:: Portlet-->
        </div>
        @endforeach
        @endif


    </div>
    @endsection
