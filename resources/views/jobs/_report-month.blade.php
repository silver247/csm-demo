@extends('layouts.app')

@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Jobs</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">#สร้างรายงานประจำเดือนใหม่</span>
        </div>
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            รายละเอียดรายงานประจำเดือน
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="POST" action="/jobs/{{$job->id}}/monthly-report/create">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group form-group-last">
                            <div class="alert alert-secondary" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                <div class="alert-text">
                                    กรุณากรอกข้อมูลในช่องที่มี <span class="text-danger">*</span> ให้ครบทุกช่อง
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        <div class="form-group row">
                            
                            <div class="col-6">
                                <label for="description" >เดือน<span class="text-danger">*</span></label>
                                <select name="report_month" class="form-control">
                                    @for($i = 1; $i <= 12; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6">
                                <label for="description" >ปี<span class="text-danger">*</span></label>
                                <select name="report_year" class="form-control">
                                    @for($i = $carbon::now()->year; $i > $carbon::now()->year - 5; $i--)
                                    <option value="{{$i}}">{{$i + 543}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">จำนวนลูกจ้าง <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="" placeholder="ระบุจำนวนลูกจ้าง" name="employees" id="employees">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">จำนวนชั่วโมงการทำงาน <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="" placeholder="ระบุจำนวนชั่วโมงการทำงาน" name="working_hours" id="working_hours">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job_name">จำนวนการเกิดอุบัติการณ์ <span class="text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" type="number" value="" placeholder="ระบุจำนวนการเกิดอุบัติการณ์" name="incident_count" id="incident_count">
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- end:: Content -->
@endsection
