<!-- begin:: Footer -->
<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            2020&nbsp;&copy;&nbsp;<a href="#" target="_blank"
                                     class="kt-link">Contractor Safety Management by Puwakit K.</a>
        </div>
        <div class="kt-footer__menu">
            
        </div>
    </div>
</div>

<!-- end:: Footer -->