<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>EGAT My Card</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
        <!-- Styles -->
    <link href="{{ asset('Polo/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('Polo/css/style.css') }}" rel="stylesheet">
    </head>
    <body>
    <!-- Body Inner -->
    <div class="body-inner">
        <!-- Section -->
        <section class="fullscreen" data-bg-parallax="{{ asset('images/bg.jpg') }}">
            <div class="container">
                <div>
                    <div class="text-center m-b-30">
                        <a href="/" class="logo">
                            <img src="{{ asset('images/logo-login.png') }}" alt="EGAT">
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 center p-50 background-white b-r-6">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <h3>Login to your Account</h3>
                            <form method="POST" id="login-form" action="{{route('login')}}">
                                @csrf
                                <div class="form-group">
                                    <label class="sr-only">เลขประจำตัวพนักงาน</label>
                                    <input type="text" class="form-control" name="id" placeholder="เลขประจำตัวพนักงาน">
                                    @error('id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="form-group m-b-5">
                                    <label class="sr-only">รหัสผ่าน Email</label>
                                    <input type="password" class="form-control" name="password"  placeholder="รหัสผ่าน Email">
                                </div>
                                
                                <div class="text-left form-group">
                                    <button type="submit" id="login-btn" class="btn">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end: Section -->
    </div>
    <!-- end: Body Inner -->
    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
    <!--Plugins-->
    <script src="{{ asset('Polo/js/jquery.js') }}"></script>
    <script src="{{ asset('Polo/js/plugins.js') }}"></script>

    <!--Template functions-->
    <script src="{{ asset('Polo/js/functions.js') }}"></script>
    <script src="{{ asset('js/loginprocess.js') }}"></script>
</body>
</html>
