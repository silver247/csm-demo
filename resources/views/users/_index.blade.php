@extends('layouts.app')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Users</h3>
        </div>

    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="row">


        <div class="col-xl-12">

            <!--begin:: Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    @if($user_list->count() > 0)
                    <table class="table table-striped-">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Total Application</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user_list as $user)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->department}}</td>
                                <td>{{$user->applications()->count()}}</td>
                                <td><a href="/user/{{$user->id}}" class="btn btn-brand btn-sm btn-upper btn-bold">details</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $user_list->links() }}
                    @else
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                        <div class="alert-text">
                            There is no application yet.
                        </div>
                    </div>
                    @endif

                </div>
            </div>

            <!--end:: Portlet-->
        </div>


    </div>
    @endsection
