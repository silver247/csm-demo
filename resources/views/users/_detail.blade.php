@extends('layouts.app')

@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">User</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">#Detail</span>
        </div>
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{$user->name}}
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <div class="kt-portlet__body">
                    @if(Request::has('re') && (Request::query('re') == "true"))
                    <div class="form-group form-group-last">
                        <div class="alert alert-success" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning kt-font-danger"></i></div>
                            <div class="alert-text">
                                <p>User Access Key ได้ถูก Reset แล้ว กรุณาแจ้งผู้ใช้งานให้ตรวจสอบที่กล่องอีเมลล์ขาเข้า</p>
                                <hr>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <label for="name" class="col-2 col-form-label">ชื่อ-นามสกุล <span class="text-danger">*</span></label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="{{$user->name}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-2 col-form-label">สังกัด <span class="text-danger">*</span></label>
                        <div class="col-10">
                            <textarea class="form-control" name="description" rows="10" readonly>{{$user->department}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            @if($user->applications()->count() > 0)
                            <table class="table table-striped-">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Application Name</th>
                                        <th>Application ID</th>
                                        <th>Status</th>
                                        <th>Today Requests</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user->applications()->get() as $app)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$app->name}}</td>
                                        <td>{{$app->id}}</td>
                                        <td><span class="btn btn-label-{{$app->status_color()}} btn-sm btn-bold btn-upper">{{$app->status->description}}</span></td>
                                        <td>{{$app->todayRequest()}}</td>
                                        <td><a href="/app/{{$app->id}}" class="btn btn-brand btn-sm btn-upper btn-bold">details</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="alert alert-secondary" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                <div class="alert-text">
                                    There is no application yet.
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                                <a href="/user" class="btn btn-secondary">Back</a>
                                @can('admin')
                                <form class="form-inline pull-right" method="POST" id="tform1" action="/reissue-userkey/{{$user->id}}">
                                    @csrf
                                    <button class="btn btn-danger" type="button" id="btnReissue">Re-issue User Key</button>
                                </form>
                                @endcan
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- end:: Content -->
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $('#btnReissue').on('click', function () {
        swal.fire({
            title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, re-issue key!'
            }).then((result) => {
                if (result.value) {
                    $('#tform1').submit();
                }
            });
        });
    });
            
</script>
@endpush
