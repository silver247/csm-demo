@component('mail::message')
# Application has been sent to approver

Application has been sent to approver.
<br>
You also can visit this link to see detail
<a href="{{$url."app/".$app->id}}">Click to view</a>
<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
