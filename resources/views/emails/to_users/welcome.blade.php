@component('mail::message')
# Welcome to Mycard Developer Panel

Here is your Developer Panel's access key.
<br>
<br>
<span style="color: #5578eb!important;">{{$access_key}}</span>
<br>
<br>
Please keep this key at the safe place. If you lose the key, please send email for re-issue key to email: puwakit.kan@egat.co.th

Enjoy coding.

Regards,<br>
{{ config('app.name') }}
@endcomponent
