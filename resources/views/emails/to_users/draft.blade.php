@component('mail::message')
# Application has been saved

Application has been saved.
<br>
You also can visit this link to see detail
<a href="{{$url."app/".$app->id}}">Click to view</a>
<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
