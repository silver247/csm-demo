@component('mail::message')
# Application has been approved

Application has been approved. You can use an API now.

You also can visit this link to see detail
<a href="{{$url."app/".$app->id}}">Click to view</a>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
