@component('mail::message')
# Application has been asked for more detail

Application has been asked for more detail, the reason is {{$app->application_status_message}}
<br>
You can contact approver via Tel: 64282 or Email: puwakit.kan@egat.co.th
<br>
Please visit this link to edit detail
<a href="{{$url."app/".$app->id}}">Click to view</a>
<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
