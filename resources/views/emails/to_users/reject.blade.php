@component('mail::message')
# Application has been sent to rejected

Application has been rejected, the reason is {{$app->application_status_message}}
<br>
You can contact approver via Tel: 64282 or Email: puwakit.kan@egat.co.th
<br>
You also can visit this link to see detail
<a href="{{$url."app/".$app->id}}">Click to view</a>
<br>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
