@component('mail::message')
# Application has been revised

Application has been revised. Please visit this link to see detail
<br>
<a href="{{$url."app/".$app->id}}">Click to view</a>
<br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
