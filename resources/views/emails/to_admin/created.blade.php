@component('mail::message')
# New application

New application has been created. Please visit this link to see detail
<br>
<a href="{{$url."app/".$app->id}}">Click to view</a>
<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
