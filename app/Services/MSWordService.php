<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Services\TemplateProcesserExtension;
use Illuminate\Support\Facades\Storage;
use Exception;

/**
 * Description of MSWordService
 *
 * @author EGAT
 */
class MSWordService {

    //put your code here

    public function __construct() {
        ;
    }

    public static function init() {
        return new MSWordService();
    }

    public function generateDocumentFromTemplate($document) {
        try {
            if (!$document->tor_template()) {
                throw new Exception('template not found');
            }
            $templateProcessor = new TemplateProcesserExtension(storage_path('app/public/' . $document->tor_template()));
            $by = date('Y', strtotime($document->job_start)) + 543;
            $by_end = date('Y', strtotime($document->job_end)) + 543;
            $templateProcessor->setValue('dept_owner', $document->department->description);
            $templateProcessor->setValue('job_name', $document->job_name);
            $duration = $document->job_start->set('year', $by)->locale('th_TH')->isoFormat('LL') . " - " . $document->job_end->set('year', $by_end)->locale('th_TH')->isoFormat('LL');
            $templateProcessor->setValue('job_duration', $duration);
            $templateProcessor->setValue('send_location', ($document->incl_location) ? 'ส่งมอบพื้นที่' : 'ไม่ได้ส่งมอบพื้นที่');
            $templateProcessor->setValue('job_location', $document->location->description);
            $templateProcessor->setValue('job_type', $document->job_type->description);

            $activities = implode(',', $document->activities()->pluck('description')->all());
            $equipments = implode(',', $document->equipments()->pluck('description')->all());
            $environments = implode(',', $document->environments()->pluck('description')->all());


            $templateProcessor->setValue('job_activity', $activities);
            $templateProcessor->setValue('job_equipment', $equipments);
            $templateProcessor->setValue('job_environment', $environments);
            $templateProcessor->setValue('risk_group', $document->risk_group()->name);

            $laws = array();
            foreach ($document->relate_laws()['normal'] as $law) {
                $tmp = array();
                $tmp['laws'] = $law;
                $laws[] = $tmp;
            }

            $laws_block_replacements = $laws;
            $templateProcessor->cloneBlockNew('law_block', 0, true, false, $laws_block_replacements);

            $egat_laws = array();
            foreach ($document->relate_laws()['egat'] as $egat) {
                $tmp = array();
                $tmp['egat_rules'] = $egat;
                $egat_laws[] = $tmp;
            }
            $egat_laws_block_replacements = $egat_laws;
            $templateProcessor->cloneBlockNew('egat_rules_block', 0, true, false, $egat_laws_block_replacements);

            /*
             * Safety Committee
             */

            $safety_commitee_list = array();
            foreach ($document->safety_committee() as $sc) {
                $tmp = array();
                $tmp['safety_commitee'] = $sc;
                $safety_commitee_list[] = $tmp;
            }

            $safety_commitee_block_replacement = $safety_commitee_list;
            $templateProcessor->cloneBlockNew('safety_commitee_block', 0, true, false, $safety_commitee_block_replacement);

            /*
             * Law Requeirement
             */

            $law_req_list = array();
            foreach ($document->relate_laws_object() as $law) {
                foreach ($law->requirements()->get() as $requirement) {
                    $tmp = array();
                    $tmp['law_requirement'] = $requirement->description;
                    $law_req_list[] = $tmp;
                }
            }

            $law_requirement_block_replacement = $law_req_list;
            $templateProcessor->cloneBlockNew('law_requirement_block', 0, true, false, $law_requirement_block_replacement);


            $path = storage_path('app/public/export_results/export-' . $document->document_ref . '.docx');
            $templateProcessor->saveAs($path);

            return $path;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
