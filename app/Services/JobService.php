<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Model\Document;
use App\Model\RiskTopicMaster;
use App\Model\RiskGroupConfig;
use App\Model\MonthReport;
use App\Model\DocumentJobCloseSurvey;

/**
 * Description of JobService
 *
 * @author puwak
 */
class JobService {

    //put your code here
    public function __construct() {
        ;
    }

    public static function init() {
        return new JobService();
    }

    public function getJobDataByID($id) {
        return Document::findOrFail($id);
    }

    public function getJobGroupByID($id) {
        $document = Document::findOrFail($id);
        $score = 0;
        foreach ($document->risks()->get() as $risk) {
            $score += $risk->score;
        }
        $group = RiskGroupConfig::where('min_score', '<=', $score)->where('max_score', '>=', $score)->first();
        return $group;
    }

    public function createNewJob($request) {
        return DB::transaction(function () use ($request) {
                    $document = new Document();
                    $document->document_ref = Str::random(16);
                    $document->owner_csct = $request->owner_csct;
                    $document->job_name = $request->job_name;
                    //$document->job_description = $request->job_description;
                    $document->job_start = $request->job_start;
                    $document->job_end = $request->job_end;
                    $document->incl_location = $request->incl_location;
                    $document->job_location_master_id = $request->job_location_master_id;
                    $document->job_location_other = $request->job_location_other;
                    $document->job_type_id = $request->job_type_master_id;
                    $document->job_type_other = $request->job_type_other;
                    $document->vendor_type_id = $request->vendor_type_id;
                    $document->vendor_employee = $request->vendor_employee;
                    $document->document_statuses_id = config('constant.job_status.normal');
                    $document->create_by = Auth::id();
                    $document->update_by = Auth::id();
                    $document->save();

                    $activity_list = array();
                    foreach ($request->job_activities as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $activity_list[] = $value;
                    }
                    $document->activities()->sync($activity_list);

                    $equipments = array();
                    foreach ($request->job_equipments as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $equipments[] = $value;
                    }
                    $document->equipments()->sync($equipments);

                    $environments = array();
                    foreach ($request->job_environments as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $environments[] = $value;
                    }
                    $document->environments()->sync($environments);

                    $risks_arr = array();
                    $topics = RiskTopicMaster::get();
                    foreach ($topics as $topic) {
                        $risks_arr[] = $request->risks[$topic->id];
                    }

                    $document->risks()->sync($risks_arr);

                    return $document->id;
                });
    }

    public function updateJob($request, $id) {
        return DB::transaction(function () use ($request, $id) {
                    $document = Document::findOrFail($id);
                    $document->owner_csct = $request->owner_csct;
                    $document->job_name = $request->job_name;
                    //$document->job_description = $request->job_description;
                    $document->job_start = $request->job_start;
                    $document->job_end = $request->job_end;
                    $document->incl_location = $request->incl_location;
                    $document->job_location_master_id = $request->job_location_master_id;
                    $document->job_location_other = $request->job_location_other;
                    $document->job_type_id = $request->job_type_master_id;
                    $document->job_type_other = $request->job_type_other;
                    $document->vendor_type_id = $request->vendor_type_id;
                    $document->vendor_employee = $request->vendor_employee;
                    $document->vendor_name = $request->vendor_name;
                    $document->vendor_contract_id = $request->vendor_contract_id;
                    $document->update_by = Auth::id();
                    $document->save();

                    $activity_list = array();
                    foreach ($request->job_activities as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $activity_list[] = $value;
                    }
                    $document->activities()->sync($activity_list);

                    $equipments = array();
                    foreach ($request->job_equipments as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $equipments[] = $value;
                    }
                    $document->equipments()->sync($equipments);

                    $environments = array();
                    foreach ($request->job_environments as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $environments[] = $value;
                    }
                    $document->environments()->sync($environments);

                    $risks_arr = array();
                    $topics = RiskTopicMaster::get();
                    foreach ($topics as $topic) {
                        $risks_arr[] = $request->risks[$topic->id];
                    }

                    $document->risks()->sync($risks_arr);
                });
    }

    public function updateJobStatus($status, $id) {
        return DB::transaction(function () use ($status, $id) {
                    $document = Document::findOrFail($id);
                    $document->document_statuses_id = $status;
                    $document->update_by = Auth::id();
                    $document->save();
                });
    }

    public function saveMonthReport($request, $id) {
        return DB::transaction(function () use ($request, $id) {
                    $document = Document::findOrFail($id);
                    $report = new MonthReport();
                    $report->job_id = $id;
                    $report->report_month = $request->report_month;
                    $report->report_year = $request->report_year;
                    $report->employees = $request->employees;
                    $report->working_hours = $request->working_hours;
                    $report->incident_count = $request->incident_count;
                    $report->create_by = Auth::id();
                    $report->update_by = Auth::id();
                    $report->save();
                    
                    return $report->id;
                });
    }

    public function saveCloseJobSurvey($request, $id) {
        return DB::transaction(function () use ($request, $id) {
                    $document = Document::findOrFail($id);
                    foreach ($request->surveys as $key => $value) {
                        if ($value === null || $value === "") {
                            continue;
                        }
                        $survey = new DocumentJobCloseSurvey();
                        $survey->document_id = $id;
                        $survey->close_job_topic_id = $key;
                        $survey->close_job_measurement_id = $value;
                        $survey->create_by = Auth::id();
                        $survey->update_by = Auth::id();
                        $survey->save();
                    }
                    $document->document_statuses_id = config('constant.job_status.complete');
                    $document->save();
                });
    }

}
