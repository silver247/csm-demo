<?php

namespace App\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Membership
 *
 * @author EGAT
 */
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Services\MailService;

class Membership {

    //put your code here
    public function __construct() {
        ;
    }

    public static function init() {
        return new Membership();
    }

    public function EGATAuth($username, $password) {
        //confidential
    }

    public function EGATUserData($username, $target_username, $hash_key, $format = 'json') {
        //confidential
    }

    public function EGATAllDeptData($username, $hash_key, $format = 'json') {
        //confidential
    }
    
    public function EGATDeptData($username, $hash_key, $cost_center, $format = 'json') {
        //confidential
    }

    public function createOrUpdateUser($emp_id, $password, $hash_key) {
        //confidential
    }
    
    public function reissueKey($user_id){
        $user = User::findOrFail($user_id);
        $user->tokens()->where('name', 'access-key')->delete();
        $token = $user->createToken('access-key');
        return $token->plainTextToken;
    }

    
    

}
