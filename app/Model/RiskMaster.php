<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RiskMaster extends Model
{
    //
    public function documents() {
        return $this->belongsToMany('App\Model\Document', 'document_risks', 'risk_master_id', 'document_id');
    }
    
    public function topic() {
        return $this->belongsTo('App\Model\RiskTopicMaster', 'risk_topic_master_id');
    }
    
    public function level() {
        return $this->belongsTo('App\Model\RiskLevel', 'risk_level_id');
    }
    
    public function count_topic($topic_id) {
        return $this->where('risk_topic_master_id', $topic_id)->count();
    }
}
