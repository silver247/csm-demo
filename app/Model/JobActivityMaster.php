<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobActivityMaster extends Model
{
    //
    public function laws() {
        return $this->belongsToMany('App\Model\LawMaster', 'law_job_relations', 'job_activity_master_id', 'law_master_id');
    }
}
