<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CloseJobMeasurement extends Model
{
    //
    public function close_job_survey() {
        return $this->hasMany('App\Model\DocumentJobCloseSurvey', 'close_job_measurement_id');
    }
}
