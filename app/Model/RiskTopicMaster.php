<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RiskTopicMaster extends Model
{
    //
    public function risks() {
        return $this->hasMany('App\Model\RiskMaster', 'risk_topic_master_id');
    }
    
}
