<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EnvironmentMaster extends Model
{
    //
    public function job_environments() {
        return $this->hasMany('App\Model\JobEnvironment', 'environment_master_id');
    }
}
