<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LawMaster extends Model
{
    //
    public function activities() {
        return $this->belongsToMany('App\Model\JobActivityMaster', 'law_job_relations', 'law_master_id', 'job_activity_master_id');
    }
    
    public function requirements() {
        return $this->belongsToMany('App\Model\Requirement', 'law_requirements', 'law_master_id', 'requirement_id');
    }
}
