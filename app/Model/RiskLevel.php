<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RiskLevel extends Model
{
    //
    public function risks() {
        return $this->hasMany('App\Model\RiskLevel', 'risk_level_id');
    }
}
