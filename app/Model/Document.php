<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Services\JobService;
use App\Model\RiskGroupDocument;

class Document extends Model
{
    //
    protected $dates = ['created_at', 'updated_at', 'job_start', 'job_end'];
    public function department() {
        return $this->belongsTo('App\Model\Department', 'owner_csct', 'cost_center');
    }
    
    public function location() {
        return $this->belongsTo('App\Model\JobLocationMaster', 'job_location_master_id');
    }
    
    public function job_type() {
        return $this->belongsTo('App\Model\JobTypeMaster', 'job_type_id');
    }
    
    public function status() {
        return $this->belongsTo('App\Model\DocumentStatus', 'document_statuses_id');
    }
    
    public function activities() {
        return $this->belongsToMany('App\Model\JobActivityMaster', 'document_job_activities', 'document_id', 'job_activity_masters_id');
    }
    
    public function equipments() {
        return $this->belongsToMany('App\Model\JobEquipment', 'document_job_equipment', 'document_id', 'job_equipment_id');
    }
    
    public function environments() {
        return $this->belongsToMany('App\Model\JobEnvironment', 'document_job_environments', 'document_id', 'job_environment_id');
    }
    
    public function risks() {
        return $this->belongsToMany('App\Model\RiskMaster', 'document_risks', 'document_id', 'risk_master_id');
    }
    
    public function vendor() {
        return $this->belongsTo('App\Model\VendorType', 'vendor_type_id');
    }
    
    public function created_by() {
        return $this->belongsTo('App\User', 'create_by');
    }
    
    public function updated_by() {
        return $this->belongsTo('App\User', 'update_by');
    }
    
    public function risk_group() {
        return JobService::init()->getJobGroupByID($this->id);
    }
    
    public function close_job_survey() {
        return $this->hasMany('App\Model\DocumentJobCloseSurvey', 'document_id');
    }
    
    public function tor_template(){
        $group = $this->risk_group();
        if(RiskGroupDocument::where('group_level', $group->group_level)->where('has_location', $this->incl_location)->exists()){
            return RiskGroupDocument::where('group_level', $group->group_level)->where('has_location', $this->incl_location)->first()->filepath;
        }else{
            return false;
        }
    }
    
    public function relate_laws(){
        $laws = array();
        $egat_laws = array();
        foreach($this->activities()->get() as $activity){
            foreach($activity->laws()->get() as $law){
                if($law->law_type_id === 1){ //1 = normal law
                    $laws[] = $law->description;
                }else{
                    $egat_laws[] = $law->description; //2 = egat law
                }
            }
        }
        $result = array(
            'normal' => $laws,
            'egat' => $egat_laws,
        );
        return $result;
    }
    
     public function relate_laws_object(){
        $laws = array();
        foreach($this->activities()->get() as $activity){
            foreach($activity->laws()->get() as $law){
                $laws[] = $law;
            }
        }
        
        return $laws;
    }
    
    public function safety_committee(){
        $commitee_list = array();
        
        if($this->vendor_employee >= 50 && $this->vendor_employee <= 99){
            $commitee_list[] = 'คณะกรรมการความปลอดภัยฯ (คปอ.) ไม่น้อยกว่า ๕ คน';
        }elseif($this->vendor_employee >= 50 && $this->vendor_employee <= 99){
            $commitee_list[] = 'คณะกรรมการความปลอดภัยฯ (คปอ.) ไม่น้อยกว่า ๗ คน';
        }elseif($this->vendor_employee >= 50 && $this->vendor_employee <= 99){
            $commitee_list[] = 'คณะกรรมการความปลอดภัยฯ (คปอ.) ไม่น้อยกว่า ๑๑ คน';
        }else{

        }
        
        if($this->vendor_type_id === 1){
            if($this->vendor_employee >= 2){
                $commitee_list[] = 'เจ้าหน้าที่ความปลอดภัย';
                $commitee_list[] = 'หน่วยงานความปลอดภัย';
            }
        }elseif($this->vendor_type_id >= 2 && $this->vendor_type_id <= 5){
            if($this->vendor_employee >= 2){
                $commitee_list[] = 'เจ้าหน้าที่ความปลอดภัย';
            }
            if($this->vendor_employee >= 200){
                $commitee_list[] = 'หน่วยงานความปลอดภัย';
            }
        }else{
            if($this->vendor_employee >= 20){
                $commitee_list[] = 'เจ้าหน้าที่ความปลอดภัย';
            }
        }
        
        return $commitee_list;
    }
    
    public function statusNormal(){
        return ($this->document_statuses_id === config('constant.job_status.normal')) ? true : false;
    }
    
    public function statusComplete(){
        return ($this->document_statuses_id === config('constant.job_status.complete')) ? true : false;
    }
    
}
