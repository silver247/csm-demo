<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Department;
use App\Model\JobLocationMaster;
use App\Model\JobTypeMaster;
use App\Model\JobActivityMaster;
use App\Model\JobEquipment;
use App\Model\JobEnvironment;
use App\Model\EnvironmentMaster;
use App\Model\RiskMaster;
use App\Services\JobService;
use App\Model\Document;
use App\Model\MonthReport;
use App\Model\CloseJobTopic;
use App\Model\CloseJobMeasurement;
use App\Model\VendorType;
use App\Model\DocumentJobCloseSurvey;
use Carbon\Carbon;
use SoapClient;
use App\Services\MSWordService;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $job_list = Document::where('create_by', Auth::id())->get();
        return view('home', compact('job_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $department_list = Department::where('org_level', '<=', 3)->get();
        $job_location_list = JobLocationMaster::get();
        $job_type_list = JobTypeMaster::get();
        $job_activity_list = JobActivityMaster::get();
        $job_equipment_list = JobEquipment::get();
        $job_environment_list = JobEnvironment::get();
        $environment_list = EnvironmentMaster::get();
        $vendor_list = VendorType::get();
        $risk_list = RiskMaster::get();
        return view('jobs._create', compact('department_list', 'job_location_list', 'job_type_list', 'job_activity_list', 'job_equipment_list', 'job_environment_list',
                'environment_list', 'risk_list', 'vendor_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id = JobService::init()->createNewJob($request);
        return redirect('/jobs/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $job = JobService::init()->getJobDataByID($id);
        $job_location_list = JobLocationMaster::get();
        $job_type_list = JobTypeMaster::get();
        $job_activity_list = JobActivityMaster::get();
        $job_equipment_list = JobEquipment::get();
        $job_environment_list = JobEnvironment::get();
        $environment_list = EnvironmentMaster::get();
        $risk_list = RiskMaster::get();
        return view('jobs._detail', compact('job', 'job_location_list', 'job_type_list', 'job_activity_list', 'job_equipment_list', 'job_environment_list',
                'environment_list', 'risk_list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $department_list = Department::where('org_level', '<=', 3)->get();
        $job = JobService::init()->getJobDataByID($id);
        $job_location_list = JobLocationMaster::get();
        $job_type_list = JobTypeMaster::get();
        $job_activity_list = JobActivityMaster::get();
        $job_equipment_list = JobEquipment::get();
        $job_environment_list = JobEnvironment::get();
        $environment_list = EnvironmentMaster::get();
        $vendor_list = VendorType::get();
        $risk_list = RiskMaster::get();
        return view('jobs._edit', compact('department_list','job', 'job_location_list', 'job_type_list', 'job_activity_list', 'job_equipment_list', 'job_environment_list',
                'environment_list', 'risk_list','vendor_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        JobService::init()->updateJob($request, $id);
        return redirect('/jobs/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function pageMonthReport($id){
        $report_list = MonthReport::paginate(20);
        $carbon = new Carbon();
        $job = JobService::init()->getJobDataByID($id);
        return view('jobs._report-month-list', compact('report_list', 'carbon', 'job'));
    }
    
    public function pageMonthReportCreate($id){
        $job = Document::findOrFail($id);
        $carbon = new Carbon();
        return view('jobs._report-month', compact('job', 'carbon'));
    }
    
    public function pageMonthReportPost(Request $request, $id){
        $report_id = JobService::init()->saveMonthReport($request, $id);
        return redirect('/jobs/'.$id.'/monthly-report/detail/'.$report_id);
    }
    
    public function pageMonthReportDetail(Request $request, $id, $report_id){
        $report = MonthReport::findOrFail($report_id);
        return view('jobs._report-month-detail', compact('report', 'id'));
    }
    
    public function pageCloseJobSurvey(Request $request, $id){
        if(DocumentJobCloseSurvey::where('document_id', $id)->exists()){
            return redirect('/jobs/'.$id.'/closejob-survey-complete/');
        }
        $job = JobService::init()->getJobDataByID($id);
        $job_location_list = JobLocationMaster::get();
        $job_type_list = JobTypeMaster::get();
        $job_activity_list = JobActivityMaster::get();
        $job_equipment_list = JobEquipment::get();
        $job_environment_list = JobEnvironment::get();
        $environment_list = EnvironmentMaster::get();
        $cj_topics = CloseJobTopic::where('active', true)->get();
        $cj_measurements = CloseJobMeasurement::where('active', true)->orderBy('score', 'desc')->get();
        $risk_list = RiskMaster::get();
        return view('jobs._close-job-survey', compact('job', 'job_location_list', 'job_type_list', 'job_activity_list', 'job_equipment_list', 'job_environment_list',
                'environment_list', 'risk_list', 'cj_topics', 'cj_measurements'));
    }
    
    public function pageCloseJobSurveyComplete(Request $request, $id){
        if(!DocumentJobCloseSurvey::where('document_id', $id)->exists()){
            return redirect('/jobs/'.$id.'/closejob-survey/');
        }
        $job = JobService::init()->getJobDataByID($id);
        $job_location_list = JobLocationMaster::get();
        $job_type_list = JobTypeMaster::get();
        $job_activity_list = JobActivityMaster::get();
        $job_equipment_list = JobEquipment::get();
        $job_environment_list = JobEnvironment::get();
        $environment_list = EnvironmentMaster::get();
        $cj_topics = CloseJobTopic::where('active', true)->get();
        $cj_measurements = CloseJobMeasurement::where('active', true)->orderBy('score', 'desc')->get();
        $risk_list = RiskMaster::get();
        $surveyObjs = DocumentJobCloseSurvey::where('document_id', $id)->get();
        $survey = array();
        foreach($surveyObjs as $row){
            $survey[$row['close_job_topic_id']] = $row['close_job_measurement_id'];
        }
        return view('jobs._close-job-survey-complete', compact('job', 'job_location_list', 'job_type_list', 'job_activity_list', 'job_equipment_list', 'job_environment_list',
                'environment_list', 'risk_list', 'cj_topics', 'cj_measurements', 'survey'));
    }
    
    public function closeJobSurveyPost(Request $request, $id){
        JobService::init()->saveCloseJobSurvey($request, $id);
        return redirect('/jobs/'.$id.'/closejob-survey/');
    }
    
    public function generateMSWord($path, $filename) {
        $header = [
            'Content-Type' =>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'Content-Disposition' =>'attachment; filename=test.docx',
            'Content-Transfer-Encoding' =>'binary',
            'Cache-Control' =>'must-revalidate, post-check=0, pre-check=0',
            'Content-Description' =>'File Transfer'
        ];
        ob_clean();
        return response()
                ->download($path, $filename, $header);
    }
    
    public function exportTOR($id){
        $document = Document::findOrFail($id);
        try{
            $path = MSWordService::init()->generateDocumentFromTemplate($document);
            return $this->generateMSWord($path, $document->risk_group()->name.'-'.$document->document_ref .'.docx');
        } catch (Exception $ex) {
            abort(400, $ex->getMessage());
        }
    }
    
}
