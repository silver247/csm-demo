<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'job_status' => [
        'normal' => 1,
        'complete' => 7,
    ],
    'job_activity' => [
        'low_air' => 1,
        'heat' => 2,
        'chemical' => 3,
        'radiation' => 4,
        'explosive' => 5,
        'high' => 6,
        'electric' => 7,
        'm_machine' => 8, //moveable
        'i_machine' => 9, //installtion
        'lift_move' => 10, 
        'fire_alarm' => 11, 
        'drill' => 12, 
        'dive' => 13, 
        'scaffold' => 14, 
        'other' => 15, 
        'management' => 16, 
        
    ],
];