<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/logout', function () {
    Auth::logout();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/setvendor', 'DocumentController@pageSetVendor');

Route::prefix('jobs')->middleware('auth')->group(function () {
    Route::get('/', 'DocumentController@index');
    Route::get('/create', 'DocumentController@create');
    Route::post('/create', 'DocumentController@store');
    Route::get('/{id}', 'DocumentController@show');
    Route::get('/{id}/exportTOR', 'DocumentController@exportTOR');
    Route::get('/{id}/edit', 'DocumentController@edit');
    Route::put('/{id}', 'DocumentController@update');
    Route::get('/{id}/monthly-report', 'DocumentController@pageMonthReport');
    Route::get('/{id}/monthly-report/create', 'DocumentController@pageMonthReportCreate');
    Route::post('/{id}/monthly-report/create', 'DocumentController@pageMonthReportPost');
    Route::get('/{id}/monthly-report/detail/{report_id}', 'DocumentController@pageMonthReportDetail');
    Route::get('/{id}/closejob-survey', 'DocumentController@pageCloseJobSurvey');
    Route::get('/{id}/closejob-survey-complete', 'DocumentController@pageCloseJobSurveyComplete');
    Route::post('/{id}/closejob-survey', 'DocumentController@closeJobSurveyPost');
});