<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('document_ref')->unique();
            $table->string('owner_csct')->index();
            $table->string('job_name')->index();
            //$table->text('job_description');
            $table->date('job_start');
            $table->date('job_end');
            $table->boolean('incl_location')->default(false);
            $table->bigInteger('job_location_master_id');
            $table->string('job_location_other');
            $table->bigInteger('job_type_id');
            $table->string('job_type_other');
            $table->bigInteger('document_statuses_id');
            $table->string('vendor_name')->nullable();
            $table->string('vendor_contract_id')->nullable();
            $table->bigInteger('vendor_type_id');
            $table->integer('vendor_employee')->default(0);
            $table->bigInteger('create_by');
            $table->bigInteger('update_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
