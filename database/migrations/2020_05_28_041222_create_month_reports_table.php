<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('month_reports', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('job_id');
            $table->integer('report_month');
            $table->integer('report_year');
            $table->integer('employees');
            $table->integer('working_hours');
            $table->integer('incident_count');
            $table->bigInteger('create_by');
            $table->bigInteger('update_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('month_reports');
    }
}
