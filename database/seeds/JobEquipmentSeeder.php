<?php

use Illuminate\Database\Seeder;

class JobEquipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = array(
            'ปั้นจั่นอยู่กับที่/เคลื่อนที่',
            'รถขุด/รถตัก/รถแทรกเตอร์/รถเกลี่ย/รถบด/รถบรรทุกเทท้าย',
            'รถบรรทุกสิบล้อ/รถบรรทุกน้ำมัน/รถบรรทุกน้ำ',
            'รถฟอร์คลิฟต์/รถกระเช้า',
            'รถลากจูง/รถเทรลเลอร์',
            'เครื่องมือไฟฟ้าชนิดถือ/ชนิดติดอยู่กับที่',
            'เครื่องเชื่อม/เครื่องกลึง/เครื่องตัด/เครื่องปั๊ม/เครื่องเจาะ',
            'อุปกรณ์ยก (รอก/โซ่/สลิง)',
            'อื่น ๆ',
        );
        foreach ($list as $row) {
            DB::table('job_equipment')->insert([
                'description' => $row,
                'active' => true,
            ]);
        }
    }
}
