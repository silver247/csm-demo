<?php

use Illuminate\Database\Seeder;

class RiskGroupConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = [
            ['name' => 'กลุ่มที่ ๑ กลุ่มงานที่มีความเสี่ยงต่ำ','min' => 0, 'max' => 39, 'level' => 1],
            ['name' => 'กลุ่มที่ ๒ กลุ่มงานที่มีความเสี่ยงปานกลาง','min' => 40, 'max' => 99, 'level' => 2],
            ['name' => 'กลุ่มที่ ๓ กลุ่มงานที่มีความเสี่ยงสูง','min' => 100, 'max' => 999, 'level' => 3],
        ];
        foreach ($list as $row) {
            DB::table('risk_group_configs')->insert([
                'name' => $row['name'],
                'min_score' => $row['min'],
                'max_score' => $row['max'],
                'group_level' => $row['level']
            ]);
        }
    }
}
