<?php

use Illuminate\Database\Seeder;

class JobEnvironmentSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        $env_list = array(
            1 => array(
                'พื้นลื่น/ลาดเอียง/สูงชัน/ขรุขระ',
                'ความสั่นสะเทือน',
                'ความร้อน',
                'แสงสว่าง',
                'เสียง',
                'อื่น ๆ',
            ),
            2 => array(
                'สารพิษ',
                'สารกัมมันตรังสี',
                'สารกัดกร่อน',
                'สารไวไฟ',
                'ฟูม',
                'ฝุ่น',
                'ไอระเหย/ละออง',
                'อื่น ๆ',
            ),
            3 => array(
                'ไวรัส',
                'แบคทีเรีย',
                'เชื้อรา',
                'พาหะนำโรค',
                'อื่น ๆ',
            ),
            4 => array(
                'การทำงานเป็นกะ',
                'การทำงานผิดท่าทาง',
                'การทำงานซ้ำ ๆ เป็นเวลานาน',
                'อื่น ๆ',
            ),
        );
        
        foreach ($env_list as $key => $env) {
            foreach ($env as $row) {
                DB::table('job_environments')->insert([
                    'environment_master_id' => $key,
                    'description' => $row,
                    'active' => true,
                ]);
            }
        }
    }

}
