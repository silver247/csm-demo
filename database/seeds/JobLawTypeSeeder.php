<?php

use Illuminate\Database\Seeder;

class JobLawTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = array(
            'กฏหมาย',
            'ระเบียบ กฟผ.',
        );
        foreach ($list as $row) {
            DB::table('law_types')->insert([
                'description' => $row,
                'active' => true,
            ]);
        }
    }
}
