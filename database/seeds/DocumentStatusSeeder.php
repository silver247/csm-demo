<?php

use Illuminate\Database\Seeder;

class DocumentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = array(
            'ปรกติ',
            'ปิดงาน',
        );
        foreach ($list as $row) {
            DB::table('document_statuses')->insert([
                'description' => $row,
            ]);
        }
    }
}
