<?php

use Illuminate\Database\Seeder;

class JobActivitySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        $activity_list = array(
            'งานเกี่ยวกับที่อับอากาศ',
            'งานที่ก่อให้เกิดความร้อน/ประกายไฟ',
            'งานสารเคมีอันตราย',
            'งานรังสี',
            'งานวัตถุระเบิด',
            'งานบนที่สูง',
            'งานที่เกี่ยวกับไฟฟ้า',
            'งานเครื่องจักรที่เคลื่อนที่ได้',
            'งานเครื่องมือ เครื่องจักรในโรงงาน',
            'งานยกและเคลื่อนย้าย',
            'งานตรวจสอบ/ซ่อมบำรุงระบบอัคคีภัย',
            'งานขุดเจาะ',
            'งานประดาน้ำ',
            'งานติดตั้งนั่งร้าน',
            'อื่น ๆ',
            'การบริหารจัดการ',
        );
        foreach ($activity_list as $row) {
            DB::table('job_activity_masters')->insert([
                'description' => $row,
                'active' => true,
            ]);
        }
    }

}
