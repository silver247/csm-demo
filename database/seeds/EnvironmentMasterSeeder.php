<?php

use Illuminate\Database\Seeder;

class EnvironmentMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('environment_masters')->insert([
            'description' => 'ด้านกายภาพ',
            'active' => true,
        ]);
        
        DB::table('environment_masters')->insert([
            'description' => 'ด้านเคมี',
            'active' => true,
        ]);
        
        DB::table('environment_masters')->insert([
            'description' => 'ด้านชีวภาพ',
            'active' => true,
        ]);
        
        DB::table('environment_masters')->insert([
            'description' => 'ด้านการยศาสตร์',
            'active' => true,
        ]);
        
        
    }
}
