<?php

use Illuminate\Database\Seeder;

class CloseJobTopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = [
            'หลักฐาน/ใบรับรอง การตรวจสอบ ทดสอบ สอบเทียบ เครื่องมือ เครื่องจักร อุปกรณ์',
            'หลักฐาน/ใบรับรองการฝึกอบรมใบอนุญาต และผลการตรวจสุขภาพตามที่กฎหมายกำหนด',
            'การปฏิบัติตามแผนงานความปลอดภัย',
            'สวมใส่อุปกรณ์อุปกรณ์คุ้มครองความปลอดภัยส่วนบุคคล(PPE) ระหว่างปฏิบัติงาน',
            'การปฏิบัติตามข้อกำหนดกฎหมาย/ระเบียบ กฎความปลอดภัยของหน่วยงาน',
            'ขณะปฏิบัติงานมีอุบัติเหตุเกิดขึ้น',
        ];
        
        foreach ($list as $row) {
            DB::table('close_job_topics')->insert([
                'description' => $row,
                'active' => true,
            ]);
        }
    }
}
