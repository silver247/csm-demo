<?php

use Illuminate\Database\Seeder;

class RiskLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = array(
            'มาก', //1
            'ปานกลาง', //2
            'น้อย', //3
            'สูง', //4
            'ต่ำ', //5
            'พื้นที่อันตราย', //6
            'พื้นที่ควบคุม', //7
            'พื้นที่ทั่วไป', //8
            'ระยะยาว', //9
            'ระยะสั้น', //10
            'เป็นครั้งคราว', //11
        );
        foreach ($list as $row) {
            DB::table('risk_levels')->insert([
                'description' => $row,
                'active' => true,
            ]);
        }
    }
}
