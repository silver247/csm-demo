<?php

use Illuminate\Database\Seeder;

class LawActivityRelationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = [
            ['law' => 1, 'activ' => 1],
            ['law' => 4, 'activ' => 1],
            ['law' => 5, 'activ' => 1],
            ['law' => 16, 'activ' => 1],
            ['law' => 17, 'activ' => 1],
            ['law' => 1, 'activ' => 2],
            ['law' => 4, 'activ' => 2],
            ['law' => 8, 'activ' => 2],
            ['law' => 10, 'activ' => 2],
            ['law' => 13, 'activ' => 2],
            ['law' => 15, 'activ' => 2],
            ['law' => 16, 'activ' => 2],
            ['law' => 17, 'activ' => 2],
            ['law' => 1, 'activ' => 3],
            ['law' => 4, 'activ' => 3],
            ['law' => 11, 'activ' => 3],
            ['law' => 16, 'activ' => 3],
            ['law' => 1, 'activ' => 4],
            ['law' => 4, 'activ' => 4],
            ['law' => 7, 'activ' => 4],
            ['law' => 16, 'activ' => 4],
            ['law' => 1, 'activ' => 5],
            ['law' => 4, 'activ' => 5],
            ['law' => 8, 'activ' => 5],
            ['law' => 11, 'activ' => 5],
            ['law' => 13, 'activ' => 5],
            ['law' => 15, 'activ' => 5],
            ['law' => 1, 'activ' => 6],
            ['law' => 4, 'activ' => 6],
            ['law' => 8, 'activ' => 6],
            ['law' => 15, 'activ' => 6],
            ['law' => 16, 'activ' => 6],
            ['law' => 1, 'activ' => 7],
            ['law' => 4, 'activ' => 7],
            ['law' => 8, 'activ' => 7],
            ['law' => 12, 'activ' => 7],
            ['law' => 15, 'activ' => 7],
            ['law' => 16, 'activ' => 7],
            ['law' => 17, 'activ' => 7],
            ['law' => 1, 'activ' => 8],
            ['law' => 4, 'activ' => 8],
            ['law' => 8, 'activ' => 8],
            ['law' => 9, 'activ' => 8],
            ['law' => 15, 'activ' => 8],
            ['law' => 16, 'activ' => 8],
            ['law' => 1, 'activ' => 9],
            ['law' => 4, 'activ' => 9],
            ['law' => 9, 'activ' => 9],
            ['law' => 13, 'activ' => 9],
            ['law' => 16, 'activ' => 9],
            ['law' => 17, 'activ' => 9],
            ['law' => 1, 'activ' => 10],
            ['law' => 4, 'activ' => 10],
            ['law' => 8, 'activ' => 10],
            ['law' => 9, 'activ' => 10],
            ['law' => 15, 'activ' => 10],
            ['law' => 16, 'activ' => 10],
            ['law' => 1, 'activ' => 11],
            ['law' => 4, 'activ' => 11],
            ['law' => 8, 'activ' => 11],
            ['law' => 10, 'activ' => 11],
            ['law' => 15, 'activ' => 11],
            ['law' => 16, 'activ' => 11],
            ['law' => 17, 'activ' => 11],
            ['law' => 1, 'activ' => 12],
            ['law' => 4, 'activ' => 12],
            ['law' => 8, 'activ' => 12],
            ['law' => 9, 'activ' => 12],
            ['law' => 13, 'activ' => 12],
            ['law' => 15, 'activ' => 12],
            ['law' => 16, 'activ' => 12],
            ['law' => 1, 'activ' => 13],
            ['law' => 4, 'activ' => 13],
            ['law' => 6, 'activ' => 13],
            ['law' => 1, 'activ' => 14],
            ['law' => 4, 'activ' => 14],
            ['law' => 8, 'activ' => 14],
            ['law' => 15, 'activ' => 14],
            ['law' => 16, 'activ' => 14],
            ['law' => 1, 'activ' => 15],
            ['law' => 2, 'activ' => 15],
            ['law' => 3, 'activ' => 15],
            ['law' => 14, 'activ' => 15],
        ];
        foreach ($list as $row) {
            DB::table('law_job_relations')->insert([
                'law_master_id' => $row['law'],
                'job_activity_master_id' => $row['activ'],
            ]);
        }
    }
}
