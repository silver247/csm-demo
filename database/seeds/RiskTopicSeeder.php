<?php

use Illuminate\Database\Seeder;

class RiskTopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = array(
            'ความสูญเสียที่เกิดขึ้น',
            'ความถี่ในการเกิดเหตุการณ์',
            'ความซับซ้อนของงานที่ปฏิบัติ',
            'ลักษณะพื้นที่',
            'ระยะเวลาการทำงาน',
        );
        foreach ($list as $row) {
            DB::table('risk_topic_masters')->insert([
                'description' => $row,
                'active' => true,
            ]);
        }
    }
}
