<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permissions')->insert([//1
            'name' => 'create',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//2
            'name' => 'update',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//3
            'name' => 'delete',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//4
            'name' => 'view',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//5
            'name' => 'approve1',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//6
            'name' => 'approve2',
            'guard_name' => 'web',
        ]);
        
        DB::table('permissions')->insert([//7
            'name' => 'approve3',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//8
            'name' => 'view-report',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//9
            'name' => 'view-report-all',
            'guard_name' => 'web',
        ]);

        DB::table('permissions')->insert([//10
            'name' => 'config',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([//1
            'name' => 'user',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([//2
            'name' => 'manager1',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([//3
            'name' => 'manager2',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([//4
            'name' => 'manager3',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([//5
            'name' => 'admin',
            'guard_name' => 'web',
        ]);

        $user_permission_list = [1,2,3,4];
        $manager1_permission_list = [1,2,3,4,5,8];
        $manager2_permission_list = [1,2,3,4,5,6,8];
        $manager3_permission_list = [1,2,3,4,5,6,7,8];
        $admin_permission_list = [1,2,3,4,5,6,7,8,9,10];

        foreach ($user_permission_list as $permission) {
            DB::table('role_has_permissions')->insert([//user
                'permission_id' => $permission,
                'role_id' => 1,
            ]);
        }
        foreach ($manager1_permission_list as $permission) {
            DB::table('role_has_permissions')->insert([//manager1
                'permission_id' => $permission,
                'role_id' => 2,
            ]);
        }
        
        foreach ($manager2_permission_list as $permission) {
            DB::table('role_has_permissions')->insert([//manager2
                'permission_id' => $permission,
                'role_id' => 3,
            ]);
        }
        
        foreach ($manager3_permission_list as $permission) {
            DB::table('role_has_permissions')->insert([//manager3
                'permission_id' => $permission,
                'role_id' => 4,
            ]);
        }
        
        foreach ($admin_permission_list as $permission) {
            DB::table('role_has_permissions')->insert([//admin
                'permission_id' => $permission,
                'role_id' => 5,
            ]);
        }
    }
}
