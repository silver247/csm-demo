<?php

use Illuminate\Database\Seeder;

class JobLocationMasterSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        DB::table('job_location_masters')->insert([
            'description' => 'โรงไฟฟ้า',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'เหมือง',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'เขื่อน',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'โครงการก่อสร้าง',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'สถานีไฟฟ้าแรงสูง',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'สำนักงานใหญ่',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'สำนักงานและโรงงาน',
            'active' => true,
        ]);
        DB::table('job_location_masters')->insert([
            'description' => 'อื่นๆ',
            'active' => true,
        ]);
    }

}
