<?php

use Illuminate\Database\Seeder;

class RiskGroupDocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = [
            ['filepath' => 'ms_word_template/group_1.docx','risk_group' => 1,'location' => 0],
            ['filepath' => 'ms_word_template/group_1_location.docx','risk_group' => 1,'location' => 1],
            ['filepath' => 'ms_word_template/group_2.docx','risk_group' => 2,'location' => 0],
            ['filepath' => 'ms_word_template/group_2_location.docx','risk_group' => 2,'location' => 1],
            ['filepath' => 'ms_word_template/group_3.docx','risk_group' => 3,'location' => 0],
            ['filepath' => 'ms_word_template/group_3_location.docx','risk_group' => 3,'location' => 1],
        ];
        foreach ($list as $row) {
            DB::table('risk_group_documents')->insert([
                'filepath' => $row['filepath'],
                'group_level' => $row['risk_group'],
                'has_location' => $row['location'],
            ]);
        }
    }
}
