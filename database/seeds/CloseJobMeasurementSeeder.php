<?php

use Illuminate\Database\Seeder;

class CloseJobMeasurementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list = [
            ['desc' => 'ไม่เกี่ยวข้อง N/A', 'score' => 0],
            ['desc' => 'ไม่เป็นไปตามเกณฑ์ และเกิดผลกระทบ (หลังระยะเวลาที่กำหนด และไม่ถูกต้องหรือไม่ครบถ้วน)', 'score' => 1],
            ['desc' => 'ไม่เป็นไปตามเกณฑ์ แต่ไม่เกิดผลกระทบ (ตามระยะเวลาที่กำหนด แต่ไม่ถูกต้องหรือไม่ครบถ้วน)', 'score' => 2],
            ['desc' => 'เป็นไปตามเกณฑ์ที่กำหนด (ตามระยะเวลาที่กำหนด และถูกต้อง ครบถ้วน)', 'score' => 3],
            ['desc' => 'ดีกว่าเกณฑ์ที่กำหนด (ก่อนระยะเวลาที่กำหนด และถูกต้อง ครบถ้วน)', 'score' => 4],
        ];
        
        foreach ($list as $row) {
            DB::table('close_job_measurements')->insert([
                'description' => $row['desc'],
                'score' => $row['score'],
                'active' => true,
            ]);
        }
    }
}
