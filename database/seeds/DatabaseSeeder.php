<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(JobLocationMasterSeeder::class);
        $this->call(JobActivitySeeder::class);
        $this->call(JobEnvironmentSeeder::class);
        $this->call(JobEquipmentSeeder::class);
        $this->call(JobLawTypeSeeder::class);
        $this->call(JobTypeSeeder::class);
        $this->call(EnvironmentMasterSeeder::class);
        $this->call(DocumentStatusSeeder::class);
        $this->call(RiskLevelSeeder::class);
        $this->call(RiskTopicSeeder::class);
        $this->call(RiskMasterSeeder::class);
        $this->call(RiskGroupConfigSeeder::class);
        $this->call(LawActivityRelationTableSeeder::class);
        $this->call(LawMasterTableSeeder::class);
        $this->call(RequirementTableSeeder::class);
        $this->call(VendorTypeTableSeeder::class);
        $this->call(RiskGroupDocumentSeeder::class);
        $this->call(CloseJobTopicSeeder::class);
        $this->call(CloseJobMeasurementSeeder::class);
        $this->call(LawRequirementSeeder::class);
    }
}
