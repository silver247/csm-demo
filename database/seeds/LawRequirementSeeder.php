<?php

use Illuminate\Database\Seeder;

class LawRequirementSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        $list = [
            ['law' => 2, 'req' => 1],
            ['law' => 5, 'req' => 2],
            ['law' => 5, 'req' => 3],
            ['law' => 6, 'req' => 4],
            ['law' => 7, 'req' => 5],
            ['law' => 7, 'req' => 6],
            ['law' => 7, 'req' => 7],
            ['law' => 8, 'req' => 8],
            ['law' => 8, 'req' => 9],
            ['law' => 8, 'req' => 10],
            ['law' => 8, 'req' => 11],
            ['law' => 8, 'req' => 12],
            ['law' => 9, 'req' => 13],
            ['law' => 9, 'req' => 14],
            ['law' => 10, 'req' => 15],
            ['law' => 11, 'req' => 16],
            ['law' => 12, 'req' => 8],
            ['law' => 12, 'req' => 17],
        ];
        
        foreach ($list as $row) {
            DB::table('law_requirements')->insert([
                'law_master_id' => $row['law'],
                'requirement_id' => $row['req'],
            ]);
        }
    }

}
